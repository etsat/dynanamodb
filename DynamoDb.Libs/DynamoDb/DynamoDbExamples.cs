﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace DynamoDb.Libs.DynamoDb
{
    public class DynamoDbExamples : IDynamoDbExamples
    {
        private readonly IAmazonDynamoDB _dynamoDbClient;
        private static readonly string tableName = "TempDynamoDbTable";

        public DynamoDbExamples(IAmazonDynamoDB dynamoDbClient)
        {
            _dynamoDbClient = dynamoDbClient;
        }

        public void CreateDynamoDbTable()
        {
            try
            {
                CreateTempTable();
            } catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void CreateTempTable()
        {
            Console.WriteLine("Creating Table");

            var request = new CreateTableRequest
            {
                AttributeDefinitions = new List<AttributeDefinition>
                {
                    new AttributeDefinition
                    {
                        AttributeName = "Id",
                        AttributeType = "N"
                    },
                    new AttributeDefinition
                    {
                        AttributeName = "ReplyDateTime",
                        AttributeType = "N"
                    },
                },
                KeySchema = new List<KeySchemaElement>
                {
                    new KeySchemaElement
                    {
                        AttributeName = "Id",
                        KeyType = "HASH" // partition key
                    },
                    new KeySchemaElement
                    {
                        AttributeName = "ReplyDateTime",
                        KeyType = "Range" // sort key
                    }
                },
                //https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/HowItWorks.ReadWriteCapacityMode.html
                //ProvisionedThroughput = new ProvisionedThroughput
                //{
                //    ReadCapacityUnits = 5,
                //    WriteCapacityUnits = 5
                //},

                // pay per request: if the database is inactive, there are no charges applied:
                BillingMode = BillingMode.PAY_PER_REQUEST,
                TableName = tableName
            };

            var response = _dynamoDbClient.CreateTableAsync(request);

            WaitUntilTableReady(tableName);
        }

        private void WaitUntilTableReady(string tableName)
        {
            string status = null;

            do
            {
                Thread.Sleep(5000);
                try
                {
                    var res = _dynamoDbClient.DescribeTableAsync(new DescribeTableRequest
                    {
                        TableName = tableName
                    });
                    status = res.Result.Table.TableStatus;
                } catch (ResourceNotFoundException)
                {

                }
            } while (status != "ACTIVE");
            {
                Console.WriteLine("Table Created Successfuly");
            }

        }
    }


}
