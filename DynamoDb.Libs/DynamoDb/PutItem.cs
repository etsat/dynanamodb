﻿using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DynamoDb.Libs.DynamoDb
{
    public class PutItem : IPutItem
    {
        private readonly IAmazonDynamoDB _dynamoDbClient;
        private static readonly string tableName = "TempDynamoDbTable";

        public PutItem(IAmazonDynamoDB dynamoDbClient)
        {
            _dynamoDbClient = dynamoDbClient;
        }

        public async Task AddNewEntry()
        {
            var queryRequest = RequestBuilder();

            await PutItemAsync(queryRequest);
        }

        private async Task PutItemAsync(PutItemRequest queryRequest)
        {
            await _dynamoDbClient.PutItemAsync(queryRequest);
        }

        PutItemRequest RequestBuilder()
        {
            var request = new PutItemRequest
            {
                TableName = tableName,
                Item = new Dictionary<string, AttributeValue>()
                {
                    // Here N is the type used in the table creation - method CreateTempTable()
                    { "Id", new AttributeValue{ N = "1"} },
                    { "ReplyDateTime", new AttributeValue{ N = "92356023562035" } },
                }
            };

            return request;
        }

    }
}
