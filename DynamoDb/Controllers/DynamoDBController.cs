﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DynamoDb.Libs.DynamoDb;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DynamoDb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DynamoDBController : ControllerBase
    {
        private readonly IDynamoDbExamples _dynamoDbExample;
        private readonly IPutItem _putItem;

        public DynamoDBController(IDynamoDbExamples dynamoDbExample, IPutItem putItem)
        {
            _dynamoDbExample = dynamoDbExample;
            _putItem = putItem;
        }

        [Route("createtable")]
        public IActionResult CreateDynamoDBTable()
        {
            _dynamoDbExample.CreateDynamoDbTable();
            return Ok();
        }

        [Route("putitem")]
        public IActionResult PutItem()
        {
            _putItem.AddNewEntry();
            return Ok();
        }
    }
}